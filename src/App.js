import React from 'react';
import Report from './Report';
import * as pbi from 'powerbi-client';
import logo from './logo.svg';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <>
        <h1>Power BI Embedded</h1>
        <Report></Report>
      </>
    );
  }
}

export default App;
