import React from 'react';
import * as pbi from 'powerbi-client';
import powerbi from 'powerbi-client/dist/powerbi';
import * as AuthenticationContext from 'adal-angular/lib/adal';

class Report extends React.Component {
  constructor(props) {
    super(props);
    /**
     * @property {string} appId - Application (client) ID
     */
    this.state = {
      error: null,
      idLoaded: false,
      config: {
        appId: 'Application (client) ID',
        reportId: 'Found in the url of your report',
        workspaceId: 'Found in the url of your report (groupId)',
      }
    };
  }

  /**
   * 1 - Get access api
   * 2 - Then use it to call Power BI API
   * 3 - Finally create embed report
   */
  embedReport = async () => {
    let accessToken = await this.getAccessToken();
    let embedToken = await this.getEmbedToken(accessToken);
    
    // get secret data from config
    const { reportId, workspaceId, appId } = this.state.config;
    // Needed for specifying token type
    const { models } = pbi;

    /**
     * Embed configuration
     * @see https://github.com/Microsoft/PowerBI-JavaScript/wiki/Embed-Configuration-Details
     */
    const config = {
      type: 'report',
      tokenType: models.TokenType.Embed,
      accessToken: embedToken,
      embedUrl: `https://app.powerbi.com/reportEmbed?reportId=${reportId}&groupId=${workspaceId}`,
      id: reportId
    };

    // Get a reference to the embedded report HTML element
    const reportContainer = document.getElementById('pbi-embedded');

    // Embed the report and display it within the div container.
    const report = window.powerbi.embed(reportContainer, config);

    console.group('%c Token details', 'color: black');
    console.log('%c accessToken :', 'color: greenyellow');
    console.log(accessToken);
    console.log('%c embedToken :', 'color: cyan');
    console.log(embedToken);
    console.group('Token details');
  };

  getAccessToken = async () => {
    /**
     * Authentication Configuration
     * @see https://github.com/AzureAD/azure-activedirectory-library-for-js/wiki/Config-authentication-context
     */
    const config = {
      clientId: this.state.config.appId,
      popUp: true,
      callback: handleLogin
    };

    // callback will be called after the login with success or failure results
    function handleLogin(errorDesc, token, error, tokenType) {
      console.log(errorDesc, token, error, tokenType);
    }

    let authContext = new AuthenticationContext(config);
    /**
     * Login the User
     * @see https://github.com/AzureAD/azure-activedirectory-library-for-js/wiki/Login-methods
     */
    let user = authContext.getCachedUser();

    // Only login user when no cache user found
    if (!user) {
      // Initiate login
      authContext.login();
    }

    let accessToken;

    authContext.acquireToken(config.clientId, function(
      errorDesc,
      token,
      error
    ) {
      if (error) {
        //acquire token failure
        if (config.popUp) {
          // If using popup flows
          authContext.acquireTokenPopup(config.clientId, null, null, function(
            errorDesc,
            token,
            error
          ) {
            if (error) {
              console.error('aquireTokenPopup failed : ', errorDesc);
            }

            accessToken = token;
          });
        } else {
          // In this case the callback passed in the Authentication request constructor will be called.
          authContext.acquireTokenRedirect(config.clientId, null, null);
        }
      } else {
        //acquired token successfully
        accessToken = token;
      }
    });

    return new Promise((resolve, reject) => {
      resolve(accessToken);
    });
  };

  getEmbedToken = async accessToken => {
    // prepare http header
    const headers = new Headers();
    const bearer = 'Bearer ' + accessToken;
    // append accesToken to header
    headers.append('Authorization', bearer);
    headers.append('Content-Type', 'application/json');

    /**
     * Report configuration
     * @see https://docs.microsoft.com/en-us/rest/api/power-bi/embedtoken/reports_generatetokeningroup#request-body
     */
    let requestBody = {
      accessLevel: 'View'
    };

    const options = {
      method: 'POST',
      headers: headers,
      body: JSON.stringify(requestBody)
    };

    const { workspaceId, reportId } = this.state.config;
    // fetch embed token
    let response = await fetch(`https://api.powerbi.com/v1.0/myorg/groups/${workspaceId}/reports/${reportId}/GenerateToken`, options);
    let embedToken = await response.json();

    return embedToken.token;
  };

  componentDidMount() {}

  render() {
    const { error, isLoaded } = this.state;

    return (
      <>
        <button onClick={this.embedReport}>Sign In</button>
        <section id="pbi-embedded"></section>
      </>
    );
  }
}

export default Report;
