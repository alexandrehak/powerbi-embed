## Getting started

These instructions will get you running on your local machine for development and testing purposes.
It explains how to embed a Power BI Report in a web page for Application owns data model.

### Prerequisites

- A Power BI Report to be embedded
- Application registered
- Azure Subscription

## How to use

Install all dependencies

```
npm i
```

Configure the config state object in report.js

```javascript
this.state = {
  error: null,
  idLoaded: false,
  config: {
    appId: 'Application (client) ID',
    reportId: 'Found in the url of your report',
    workspaceId: 'Found in the url of your report (groupId)'
  }
};
```

Run the application

```
npm start
```

## Documention used

- [Embedded analytics with Power BI](https://docs.microsoft.com/en-us/power-bi/developer/embedding)
- [Power BI Embedded Playground](https://microsoft.github.io/PowerBI-JavaScript/demo/v2-demo/index.html)
- [Embed Token - Reports GenerateTokenInGroup](https://docs.microsoft.com/en-us/rest/api/power-bi/embedtoken/reports_generatetokeningroup#tokenaccesslevel)
